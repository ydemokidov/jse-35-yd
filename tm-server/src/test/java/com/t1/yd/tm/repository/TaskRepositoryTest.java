package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.ALL_PROJECTS;
import static com.t1.yd.tm.constant.TaskTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private ITaskRepository repository;

    @Before
    public void initRepository() {
        repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void add() {
        repository.add(USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1, repository.findOneById(USER1_PROJECT1_TASK1.getId()));
    }

    @Test
    public void addWithUser() {
        repository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1, repository.findOneById(USER1_PROJECT1_TASK1.getId()));
    }

    @Test
    public void addAll() {
        repository.add(ALL_TASKS);
        Assert.assertEquals(ALL_TASKS.size(), repository.findAll().size());
        Assert.assertEquals(USER1_PROJECT1_TASK1, repository.findOneById(USER1_PROJECT1_TASK1.getId()));
    }

    @Test
    public void removeById() {
        repository.add(ALL_TASKS);
        repository.removeById(USER1_PROJECT1_TASK2.getId());
        Assert.assertEquals(ALL_PROJECTS.size() - 1, repository.findAll().size());
        Assert.assertNull(repository.findOneById(USER1_PROJECT1_TASK2.getId()));
    }

    @Test
    public void removeByIndex() {
        repository.add(ALL_TASKS);
        repository.removeByIndex(1);
        Assert.assertEquals(ALL_TASKS.size() - 1, repository.findAll().size());
        Assert.assertNotEquals(USER1_PROJECT1_TASK2, repository.findOneByIndex(1));
    }

    @Test
    public void clear() {
        repository.add(ALL_TASKS);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        repository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        repository.add(USER1.getId(), USER1_PROJECT1_TASK2);
        repository.add(ADMIN.getId(), ADMIN_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASKS.size(), repository.findAll(USER1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        repository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        repository.add(USER1.getId(), USER1_PROJECT1_TASK2);
        repository.add(ADMIN.getId(), ADMIN_PROJECT1_TASK1);

        Assert.assertEquals(USER1_PROJECT1_TASK1, repository.findOneById(USER1.getId(), USER1_PROJECT1_TASK1.getId()));
        Assert.assertNull(repository.findOneById(ADMIN.getId(), USER1_PROJECT1_TASK1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        repository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        Assert.assertEquals(USER1_PROJECT1_TASK1, repository.findOneByIndex(USER1.getId(), 0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void findOneByIndexWithUserIdNegative() {
        repository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        repository.findOneByIndex(ADMIN.getId(), 0);
    }

    @Test
    public void existsById() {
        repository.add(USER1_PROJECT1_TASK1);
        Assert.assertTrue(repository.existsById(USER1_PROJECT1_TASK1.getId()));
        Assert.assertFalse(repository.existsById(USER1_PROJECT1_TASK2.getId()));
    }

}
