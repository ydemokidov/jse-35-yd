package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.USER1_PROJECT1;
import static com.t1.yd.tm.constant.TaskTestData.USER1_PROJECT1_TASK1;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;
    private IProjectTaskService service;

    @Before
    public void initRepository() {
        projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());

        taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());

        service = new ProjectTaskService(taskRepository, projectRepository);
    }

    @Test
    public void bindTaskToProject() {
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        taskRepository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1_TASK1.getId(), taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).get(0).getId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        taskRepository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        service.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        Assert.assertTrue(taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
    }

    @Test
    public void removeProjectById() {
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        taskRepository.add(USER1.getId(), USER1_PROJECT1_TASK1);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1_TASK1.getId(), USER1_PROJECT1.getId());
        service.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertTrue(taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
    }

}
