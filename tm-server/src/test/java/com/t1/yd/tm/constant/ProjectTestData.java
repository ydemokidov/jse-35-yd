package com.t1.yd.tm.constant;

import com.t1.yd.tm.model.Project;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class ProjectTestData {

    @NotNull
    public final static Project USER1_PROJECT1 = new Project();

    @NotNull
    public final static Project USER1_PROJECT2 = new Project();

    @NotNull
    public final static Project ADMIN_PROJECT1 = new Project();

    @NotNull
    public final static Project ADMIN_PROJECT2 = new Project();

    @NotNull
    public final static String PROJECT_NAME = "project1 name";

    @NotNull
    public final static String PROJECT_DESCRIPTION = "project1 desc";

    @NotNull
    public final static List<Project> ALL_PROJECTS = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2, ADMIN_PROJECT1, ADMIN_PROJECT2);

    @NotNull
    public final static List<Project> USER1_PROJECTS = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2);

    @NotNull
    public final static List<Project> ADMIN_PROJECTS = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

}
