package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.stream()
                .filter(e -> (projectId.equals(e.getProjectId()) && userId.equals(e.getUserId())))
                .collect(Collectors.toList());
    }

}
