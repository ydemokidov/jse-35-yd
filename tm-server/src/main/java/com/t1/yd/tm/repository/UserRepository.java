package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role) {
        @NotNull final User user = create(login, password, email);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return entities.stream().anyMatch(e -> login.equals(e.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return entities.stream().anyMatch(e -> email.equals(e.getEmail()));
    }

}
