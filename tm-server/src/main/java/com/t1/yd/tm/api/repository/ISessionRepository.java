package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
