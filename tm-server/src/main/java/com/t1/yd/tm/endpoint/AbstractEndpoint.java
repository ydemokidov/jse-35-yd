package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.AbstractUserRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.model.Session;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public class AbstractEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final Session session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final Role userRole = session.getRole();
        if (userRole == null) throw new AccessDeniedException();
        final boolean check = userRole.equals(role);
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return serviceLocator.getAuthService().validateToken(token);
    }


}
