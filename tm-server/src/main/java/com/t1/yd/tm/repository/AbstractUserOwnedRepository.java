package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<E extends AbstractUserOwnedEntity> extends AbstractRepository<E> implements IUserOwnedRepository<E> {

    @Override
    @NotNull
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> foundEntities = findAll(userId);
        entities.removeAll(foundEntities);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        final List<E> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) return null;
        if (userId.isEmpty()) return null;
        return entities.stream()
                .filter(e -> (userId.equals(e.getUserId()) && id.equals(e.getId())))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<E> userEntities = findAll(userId);
        @NotNull final E entityToRemove = userEntities.get(index);
        removeById(entityToRemove.getId());
        return entityToRemove;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

}
