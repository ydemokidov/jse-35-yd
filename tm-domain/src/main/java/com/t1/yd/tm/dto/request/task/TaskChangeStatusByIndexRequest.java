package com.t1.yd.tm.dto.request.task;

import com.t1.yd.tm.dto.request.AbstractUserRequestByIndex;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskChangeStatusByIndexRequest extends AbstractUserRequestByIndex {

    private String status;

}
