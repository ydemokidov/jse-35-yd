package com.t1.yd.tm.dto.response.system;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class ArgumentListResponse extends AbstractResultResponse {

    private List<String> arguments;

    public ArgumentListResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public ArgumentListResponse() {
    }

}
