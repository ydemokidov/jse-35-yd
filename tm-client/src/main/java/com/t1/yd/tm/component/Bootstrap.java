package com.t1.yd.tm.component;

import com.t1.yd.tm.api.endpoint.*;
import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.exception.system.ArgumentNotSupportedException;
import com.t1.yd.tm.exception.system.CommandNotSupportedException;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.service.CommandService;
import com.t1.yd.tm.service.LoggerService;
import com.t1.yd.tm.service.PropertyService;
import com.t1.yd.tm.service.TokenService;
import com.t1.yd.tm.util.SystemUtil;
import com.t1.yd.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.yd.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IDataEndpoint dataEndpointClient = IDataEndpoint.newInstance(propertyService);

    @NotNull
    private final ISystemEndpoint systemEndpointClient = ISystemEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);


    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandClasses) registry(clazz);
    }

    public void run(@NotNull final String[] args) {
        prepareStartup();
        processArguments(args);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                e.getMessage();
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void prepareStartup() {
        initLogger();
        initPID();
        initFileScanner();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        if (args[0] == null) return;
        processArgument(args[0]);
    }

    private void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException();
        command.execute();
    }

    public void processCommand(@NotNull final String commandName) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        command.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }


    private void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    private void exit() {
        System.exit(0);
    }


}